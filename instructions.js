'use strict';

const path = require('path');

module.exports = async (cli) => {
  try {
    console.log('???');
    console.log(cli.helpers);
    const fromPath = path.join(__dirname, 'hooks.js');
    const toPath = path.join(cli.helpers.startPath(), 'hooks.js');
    await cli.copy(fromPath, toPath);
    cli.command.completed('create', 'start/hooks.js');
  } catch (e) {
    cli.command.info('start/hooks.js already exists. Copy hook implementation from following url');
    console.log('');
  }
};
