'use strict';

const { ServiceProvider } = require('@adonisjs/fold');

class SocialProvider extends ServiceProvider {
  register() {
    this.app.singleton('Ally/SocialServices', () => {
      return new (require('../src/ServicesManager'))();
    });
  }

  boot() {
    const AllyServices = this.app.use('Ally/SocialServices');
    Object.entries(AllyServices.drivers).forEach(([ key, value ]) => {
      this.app.extend('Adonis/Addons/Ally', key, () => {
        return this.app.make(value);
      });
    });
  }
}

module.exports = SocialProvider;
