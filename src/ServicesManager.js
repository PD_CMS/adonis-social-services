'use strict';

const Drivers = require('./Drivers');

class ServicesManager {
  constructor() {
    this._drivers = {};
  }

  get drivers() {
    return this._drivers;
  }

  use(name) {
    if (Drivers[name]) {
      this._drivers[name] = Drivers[name];
    }

    return this;
  }
}

module.exports = ServicesManager;
