'use strict';

const got = require('got');
const CE = require('@adonisjs/ally/src/Exceptions');
const OAuth2Scheme = require('@adonisjs/ally/src/Schemes/OAuth2');
const AllyUser = require('@adonisjs/ally/src/AllyUser');
const utils = require('@adonisjs/ally/lib/utils');
const get = require('lodash/get');
const isString = require('lodash/isString');

const acceptTemplate = apiVersion => `Accept: application/vnd.twitchtv.${apiVersion}+json`;
const authorizationTemplate = token => `OAuth ${token}`;

class Twitch extends OAuth2Scheme {
  constructor(Config) {
    const config = Config.get('services.ally.twitch');

    utils.validateDriverConfig('twitch', config);
    utils.debug('twitch', config);

    super(config.clientId, config.clientSecret, config.headers);

    this._redirectUri = config.redirectUri;
    this._redirectUriOptions = Object.assign({ response_type: 'code' }, config.options);
    this._scope = this._getInitialScopes(config.scope);
    this._fields = this._getInitialFields(config.fields);
    this._apiVersion = this._getInitialApiVersion(config.apiVersion);
    this.config = config;
  }

  static get inject() {
    return [ 'Adonis/Src/Config' ];
  }

  get scopeSeperator() {
    return ' ';
  }

  get baseUrl() {
    return 'https://id.twitch.tv';
  }

  get authorizeUrl() {
    return 'oauth2/authorize';
  }

  get accessTokenUrl() {
    return 'oauth2/token';
  }

  _getInitialApiVersion(version = 'v5') {
    return version;
  }

  _getInitialScopes(scopes = [ 'user_read' ]) {
    return scopes;
  }

  _getInitialFields(fields = [ 'name', 'email' ]) {
    return fields;
  }

  async _getUserProfile(accessToken) {
    const apiUrl = 'https://api.twitch.tv/kraken';
    const response = await got(`${apiUrl}/user`, {
      headers: {
        Accept: acceptTemplate(this._apiVersion),
        Authorization: authorizationTemplate(accessToken),
        'Client-ID': this.config.clientId,
      },
      json: true,
    });
    return response.body;
  }

  async getRedirectUrl(scope) {
    scope = scope && scope.length !== 0 ? scope : this._scope;
    return this.getUrl(this._redirectUri, scope, this._redirectUriOptions);
  }

  parseProviderResultError(error) {
    const parsedError = isString(error.data) ? JSON.parse(error.data) : null;
    const message = get(parsedError, 'message', error);
    return CE.OAuthException.tokenExchangeException(message, error.statusCode, parsedError);
  }

  parseRedirectError(queryParams) {
    return queryParams.error_message || 'Oauth failed during redirect';
  }

  async getUser(queryParams) {
    const code = queryParams.code;

    if (!code) {
      const errorMessage = this.parseRedirectError(queryParams);
      throw CE.OAuthException.tokenExchangeException(errorMessage, null, errorMessage);
    }
    const accessTokenResponse = await this.getAccessToken(code, this._redirectUri, {
      grant_type: 'authorization_code',
    });
    const userProfile = await this._getUserProfile(accessTokenResponse.accessToken);

    const user = new AllyUser();
    user
      .setOriginal(userProfile)
      .setFields(
        userProfile._id,
        userProfile.display_name,
        userProfile.email,
        userProfile.name,
        userProfile.logo
      )
      .setToken(
        accessTokenResponse.accessToken,
        accessTokenResponse.refreshToken,
        null,
        Number(accessTokenResponse.result.expires_in)
      );

    return user;
  }
}

module.exports = Twitch;
