const { hooks } = require('@adonisjs/ignitor');

hooks.after.providersRegistered(() => {
  const AllyServices = use('Ally/SocialServices');
  AllyServices.use('twitch');
});
